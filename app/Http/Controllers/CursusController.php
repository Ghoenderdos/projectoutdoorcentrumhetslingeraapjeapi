<?php

namespace App\Http\Controllers;

use App\Cursus;
use App\CursusType;
use App\User;
use Illuminate\Http\Request;

class CursusController extends Controller
{
    public function index(){
        $cursusses = Cursus::all();

        $cursusTypes = CursusType::all();
        $users = User::all();

        $data = [$cursusTypes, $users];
        return view('base.allCursus',compact('cursusses' ),compact('data'));
    }

    public function newCursus(){
        $cursusTypes = CursusType::all();
        $users = User::all();

        $data = [$cursusTypes, $users];
        return view('base.createCursus', compact('data'));
    }

    public function store(Request $request)
    {
        $cursus = new \App\Cursus;
        $cursus->timestamps = false;
        $cursus->cursusTypeId = $request->get('cursusTypeId');
        $cursus->datum = $request->get('datum');
        $cursus->tijdstip = $request->get('tijdstip');
        $cursus->maxAantal = $request->get('maxAantal');
        $cursus->instructeur = $request->get('instructeur');
        $cursus->opmerking = $request->get('opmerking');

        $cursus->save();

        $cursusTypes = CursusType::all();
        $users = User::all();

        $data = [$cursusTypes, $users];

        $cursusses = Cursus::all();

        return view('base.allCursus', compact('cursusses'), compact('data'));
    }

    public function show($id){
        $cursus = Cursus::findOrFail($id);
        return $cursus;
    }

    public function updateForm($id){
        $cursus = Cursus::findOrFail($id);
        $cursusTypes = CursusType::all();
        $users = User::all();

        $data = [$cursusTypes, $users];

        return view('base.updateCursus',compact('cursus'),compact('data'));
    }
    public function updateCursus(Request $request, $id){
        $cursus = Cursus::findOrFail($id);
        $cursus->timestamps = false;
        $cursus->cursusTypeId = $request->get('cursusTypeId');
        $cursus->datum = $request->get('datum');
        $cursus->tijdstip = $request->get('tijdstip');
        $cursus->maxAantal = $request->get('maxAantal');
        $cursus->instructeur = $request->get('instructeur');
        $cursus->opmerking = $request->get('opmerking');
        $cursus->save();

        $cursusTypes = CursusType::all();
        $users = User::all();

        $data = [$cursusTypes, $users];
        $cursusses = Cursus::all();
        return view('base.allCursus',compact('cursusses'), compact('data'));
    }

    public function destroy($id){
        $cursus = Cursus::findOrFail($id);
        $cursus->delete();

        return json_encode(['message' => 'success']);
    }

    public function getBootcamp()
    {
        $cursusses = Cursus::all();
        $cursusTypes = CursusType::all();
        return view('base.cursussen.bootcamp', compact('cursusses'), compact('cursusTypes'));
    }

    public function getKanoen()
    {
        $cursusses = Cursus::all();
        $cursusTypes = CursusType::all();
        return view('base.cursussen.kanoen', compact('cursusses'), compact('cursusTypes'));
    }

    public function getMountainbike()
    {
        $cursusses = Cursus::all();
        $cursusTypes = CursusType::all();
        return view('base.cursussen.mountainbike', compact('cursusses'), compact('cursusTypes'));
    }

    public function getKlimmen()
    {
        $cursusses = Cursus::all();
        $cursusTypes = CursusType::all();
        return view('base.cursussen.klimmen', compact('cursusses'), compact('cursusTypes'));
    }







}

