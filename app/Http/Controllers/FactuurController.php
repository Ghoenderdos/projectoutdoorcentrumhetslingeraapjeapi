<?php

namespace App\Http\Controllers;

use App\Cursus;
use App\CursusType;
use App\Factuur;
use App\FactuurRegel;
use App\User;
use function GuzzleHttp\Psr7\modify_request;
use Illuminate\Http\Request;

class FactuurController extends Controller
{
    public function index(){
        $factuur = Factuur::all();

        return $factuur;
    }

    public function store(Request $request)
    {
        $factuur = new \App\Factuur;
        $factuur->factuurId=$request->get('factuurId');
        $factuur->userId=$request->get('userId');
        $factuur->date=$request->get('date');

        $factuur->save();

        return json_encode(['message' => 'success']);
    }

    public function create($userId, $date)
    {
        $factuur = new \App\Factuur;
        $factuur->userId=$userId;
        $factuur->date=$date;

        $factuur->save();
    }


    public function showFactuursUser(){
        $factuurs = Factuur::all();

        return view('user.factuur',compact('factuurs'));
    }

    public function show($id){

        $factuur = Factuur::select('*')->where('factuurId', $id)->get();
        $users = User::all();
        $factuurRegels = FactuurRegel::select('*')->where('factuurId', $id)->get();
        $cursusses = Cursus::all();
        $cursusTypes = CursusType::all();

        return view('user.showFactuur')->with('factuur' , $factuur)
            ->with('users' , $users)
            ->with('factuurRegels' , $factuurRegels)
            ->with('cursussus' , $cursusses)
            ->with('cursusTypes' , $cursusTypes);
    }

    public function update(Request $request, $id){
        $factuur = Factuur::findOrFail($id);
        $factuur->factuurId=$request->get('factuurId');
        $factuur->userId=$request->get('userId');
        $factuur->date=$request->get('date');
        $factuur->update();

        return json_encode(['message' => 'success']);
    }

    public function destroy($id){
        $factuur = Factuur::findOrFail($id);
        $factuur->delete();

        return json_encode(['message' => 'success']);
    }
}
