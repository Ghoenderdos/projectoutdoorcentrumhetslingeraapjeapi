<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cursus extends Model
{
    protected $table = 'cursus';

    public function CursusType(){
        return $this->belongsTo('App\CursusType', 'cursusTypeId');
    }
}
