<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Factuur extends Model
{

    protected $table = 'factuur';

    public function User(){
        return $this->belongsTo('App\User', 'factuurId');
    }

    public function factuurRegels(){
        return $this->hasMany('App\FactuurRegel');
    }

    public static function findByFactuurId($id){
        $test = $id;
        echo ($test);
        $results = DB::select( DB::raw("SELECT * FROM factuur WHERE 'factuurId' = $test") );
        var_dump($results);

        return $results;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'factuurId', 'userId', 'date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
