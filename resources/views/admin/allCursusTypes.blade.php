@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="#" class="btn btn-primary" onclick="printDiv('printable')">Print</a>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div id="printable">
            <div class="row justify-content-center">
                <h1>Alle Cursus types</h1>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Naam</th>
                        <th>Prijs</th>
                        <th>Max aantal deelnemers</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cursusTypes as $cursusType)
                    <tr>
                        <td>
                            <a class="nav-link" href="{{ route('update', ['id' => $cursusType->id]) }}">{{$cursusType->naam}}</a>
                        </td>
                        <td>
                            {{$cursusType->prijs / 100}}
                        </td>
                        <td>
                            {{$cursusType->maxAantal}}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script >
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
