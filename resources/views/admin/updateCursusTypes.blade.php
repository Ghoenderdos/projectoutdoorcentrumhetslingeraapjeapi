@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Bijwerken cursus type') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('updateCursusType', ['id' => $cursusTypes->id]) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="naam" class="col-md-4 col-form-label text-md-right">{{ __('Naam') }}</label>

                                <div class="col-md-6">
                                    <input id="naam" type="text" class="form-control{{ $errors->has('naam') ? ' is-invalid' : '' }}" name="naam" value="{{ $cursusTypes->naam }}" required autofocus>

                                    @if ($errors->has('naam'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('naam') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prijs" class="col-md-4 col-form-label text-md-right">{{ __('Prijs') }}</label>

                                <div class="col-md-6">
                                    <input id="prijs" type="number" class="form-control{{ $errors->has('prijs') ? ' is-invalid' : '' }}" name="prijs" value="{{ $cursusTypes->prijs /  100}}" required autofocus>

                                    @if ($errors->has('prijs'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('prijs') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="maxAantal" class="col-md-4 col-form-label text-md-right">{{ __('Max aantal deelnemers') }}</label>

                                <div class="col-md-6">
                                    <input id="maxAantal" type="number" class="form-control{{ $errors->has('maxAantal') ? ' is-invalid' : '' }}" name="maxAantal" value="{{ $cursusTypes->maxAantal}}" required autofocus>

                                    @if ($errors->has('maxAantal'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('maxAantal') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Bijwerken') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
