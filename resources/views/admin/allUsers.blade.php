@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="#" class="btn btn-primary" onclick="printDiv('printable')">Print</a>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div id="printable">
            <div class="row justify-content-center">
                <h1>Alle gebruikers</h1>
                <a class="nav-link" href="{{ route('createAdmin') }}">nieuwe gebruiker</a>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Voornaam</th>
                        <th>Tussenvoegsel</th>
                        <th>Achternaam</th>
                        <th>Geboortedatum</th>
                        <th>Woonplaats</th>
                        <th>Telefoon</th>
                        <th>Adres</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th>delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>
                            <a class="nav-link" href="{{ route('showAdmin', ['id' => $user->id]) }}">{{$user->voornaam}}</a>
                        </td>
                        <@if($user->tussenvoegsel === '')
                            <td>
                                {{$user->tussenvoegsel}}
                            </td>
                        @endif
                        <td>
                            {{$user->achternaam}}
                        </td>
                        <td>
                            {{$user->geboortedatum}}
                        </td>
                        <td>
                            {{$user->woonplaats}}
                        </td>
                        <td>
                            {{$user->telefoon}}
                        </td>
                        <td>
                            {{$user->postcode}}
                        </td>
                        <td>
                            {{$user->adres}}
                        </td>
                        <td>
                            {{$user->email}}
                        </td>
                        @if($user->rol == 1)
                            <td>
                                Gebruiker
                            </td>
                        @elseif($user->rol == 2)
                            <td>
                                Instructeur
                            </td>
                        @elseif($user->rol == 3)
                            <td>
                                Admin
                            </td>
                        @endif
                        <td>
                            <a class="nav-link" href="{{ route('destroyAdmin', ['id' => $user->id]) }}">X</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <script >
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
