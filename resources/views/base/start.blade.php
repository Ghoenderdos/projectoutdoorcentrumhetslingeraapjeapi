@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="row justify-content-center"> <h1>Outdoorcentrum Het Slingeraapje</h1></div>
    <div class="row">
        <div class="col-md-6">
            <p>
                Alle cursussen worden gehouden op of rondom het terrein van outdoor centrum het slingeraapje: de
                les kanoën word op de vecht gegeven en begint aan de oever van het landgoed, de les mountainbiken word
                gegeven in het bos achter het landgoed, de les klimmen word gegeven in de klimhal op het landgoed en de
                les bootcamp word op het landgoed zelf gegeven..
            <ul>
                <li>
                    Mountainbike
                </li>
                <li>
                    Bootcamp
                </li>
                <li>
                    Klimmen
                </li>
                <li>
                    Kanoen
                </li>
            </ul>
            </p>
        </div>
        <div class="col-md-6">
            <img src="https://www.jezoektietsleuks.nl/static/upload/location_jzil/de039202-8856-593a-1134-df60658195d9/foto+Silke+horeca.jpg" width="500px" alt="foto outdoorcentrum">
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">

        </div>
        <div class="col-md-6">

        </div>
    </div>


</div>
@endsection
