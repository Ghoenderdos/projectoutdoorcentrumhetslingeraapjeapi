@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="row justify-content-center text-center">
        <h1>Bedrijfsgegevens</h1>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h3>Outdoorcentrum Het Slingeraapje</h3><br>
            Buitenveldweg 1<br>
            7777 XL Hardenberg<br>
            Tel.: (0548) 911 911<br>
        </div>
        <div class="col-md-9">
            <div style="width: 100%"><iframe width="100%" height="400"
            src="https://maps.google.com/maps?width=100%&height=400&hl=nl&coord=52.5772182,-6.5896247&q=Korte%20Spruit%2018%2C%20Hardenberg%2C%20Netherlands+(Outdoorcentrum%20Het%20Slingeraapje)&ie=UTF8&t=&z=13&iwloc=A&output=embed"
             frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><br />
        </div>
    </div>


    <div class="row justify-content-center">
        <h1>Trainers</h1>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-4 text-center justify-content-center">
            <img src="https://d1vzde6oamqj3g.cloudfront.net/projects/1/400x400/39461f08abb9a997c7e0c7ec1bb55584051f104c585ed1039f337694b61bef47-image-trainer-yalmard.jpg" class="rounded-circle w-75">
            <h3>Bas Tenzig</h3>
        </div>
        <div class="col-md-4 text-center justify-content-center">
            <img src="https://d1vzde6oamqj3g.cloudfront.net/projects/1/400x400/f36e9d14cce70afbf884baf699ebce61827df3648e84dd73de1a58cdc2ec6c98-image-trainer-alain.jpg" class="rounded-circle w-75">
            <h3>Ed Heuvelrijk</h3>
        </div>
        <div class="col-md-4 text-center justify-content-center">
            <img src="https://d1vzde6oamqj3g.cloudfront.net/projects/1/400x400/cce9110a0444e732266284c979c72cce3202f717592580b8f045bf535cede9ad-image-trainer-danny.jpg" class="rounded-circle w-75">
            <h3>Epke Zonderland</h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-4 text-center justify-content-center">
            <img src="https://d1vzde6oamqj3g.cloudfront.net/projects/1/400x400/86c207554d6ef0174c109f960bdc9c6d9382299412ee9de33f4157478af72718-image-trainer-wilma.jpg" class="rounded-circle w-75">
            <h3>Daphne Metland</h3>
        </div>
        <div class="col-md-4 text-center justify-content-center">
            <img src="https://cdn.shopify.com/s/files/1/1622/0319/products/20180411002046_96f24366_400x.jpeg?v=1540601430" class="rounded-circle w-75">
            <h3>Gerard Spaan</h3>
        </div>
        <div class="col-md-4 text-center justify-content-center">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4mjyrQeFyzf0WTmI6Ofa5DiWWnqS6eBDeANNFHB9n1AChT3wO" class="rounded-circle w-75">
            <h3>Bart Brentjes</h3>
        </div>
    </div>




</div>
@endsection
