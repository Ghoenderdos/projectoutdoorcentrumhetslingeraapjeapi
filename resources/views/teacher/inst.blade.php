@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <section style="background: pink;">
                    <h1>Mijn pagina</h1>
                    <ul>
                        <li>
                            <a class="nav-link" href="{{ route('showInst', ['id' => session('user.id')]) }}">{{ __('Gebruikers gegevens') }}</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('getFactuurs') }}">{{ __('Facturen') }}</a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
@endsection
