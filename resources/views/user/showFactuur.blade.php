@extends('layouts.app')

@section('content')
<div class="container">
    <a href="#" class="btn btn-primary" onclick="printDiv('printable')">Print</a>
    <div class="row justify-content-center">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>
    <div id="printable">
    <div class="row">

        <div class="col-md-8">
            <h2>Maandfactuur</h2>
        </div>
        <div class="col-md-4" style="background-color: lightcoral; border: red solid 3px;">
            <h3>Outdoorcentrum Slingeraapje</h3>
            Buitenveldweg 1<br>
            7777 XL Hardenberg<br>
            Tel.: (0548) 911 911<br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            FactuurNr:<br>
            CursistNr:
        </div>
        <div class="col-md-3">
            @foreach($factuur as $item)
                {{$item->factuurId}}
            @endforeach<br>
            {{ Session::get('user.id')}}
        </div>
        <div class="col-md-3">
            Factuur datum:
        </div>
        <div class="col-md-3">
            @foreach($factuur as $item)
                {{$item->date}}
            @endforeach
        </div>
            <table class="table">
                <thead>
                <tr>
                    <th>Cursustype</th>
                    <th>Datum</th>
                    <th>Tijd</th>
                    <th>Instructeur</th>
                    <th>Opmerking</th>
                    <th>Prijs</th>
                    <th>Totaal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($factuurRegels as $factuurRegel)
                    <tr>
                        <td>
                            @foreach($cursussus as $cursus)
                                @if($cursus->id === $factuurRegel->cursusId)
                                    @foreach($cursusTypes as $cursusType)
                                        @if($cursusType->id === $cursus->cursusTypeId)
                                            {{$cursusType->naam}}
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </td>
                        <td>
                            @foreach($cursussus as $cursus)
                                @if($cursus->id === $factuurRegel->cursusId)
                                    {{$cursus->datum}}
                                @endif
                            @endforeach
                        </td>
                        <td>
                            @foreach($cursussus as $cursus)
                                @if($cursus->id === $factuurRegel->cursusId)
                                    {{$cursus->tijdstip}}
                                @endif
                            @endforeach
                        </td>
                        <td>
                            @foreach($cursussus as $cursus)
                                @if($cursus->id === $factuurRegel->cursusId)
                                    @foreach($users as $user)
                                        @if($user->id == $cursus->instructeur)
                                            {{$user->voornaam}} {{$user->tussenvoegsel}} {{$user->achternaam}}
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </td>
                        <td>
                            @foreach($cursussus as $cursus)
                                @if($cursus->id === $factuurRegel->cursusId)
                                    {{$cursus->opmerking}}
                                @endif
                            @endforeach

                        <td>
                            @foreach($cursussus as $cursus)
                                @if($cursus->id === $factuurRegel->cursusId)
                                    @foreach($cursusTypes as $cursusType)
                                        @if($cursusType->id === $cursus->cursusTypeId)
                                            {{$cursusType->prijs}}
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </td>
                        <td>
                            @foreach($cursussus as $cursus)
                                @if($cursus->id === $factuurRegel->cursusId)
                                    @foreach($cursusTypes as $cursusType)
                                        @if($cursusType->id === $cursus->cursusTypeId)
                                            {{$cursusType->prijs }}
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="row">
            <div class="col-md-8">
            </div>
            <div class="col-md-4 text-right">
                Totaal:

                {{--@php--}}
                    {{--$test = $data[5] + $cursusType->prijs--}}
                {{--@endphp--}}

                {{--&euro;{{$test / 100}}<br>--}}
                {{--Btw 21&percnt;:&euro;{{($test / 100) / 100 * 21 }}--}}
            </div>
        </div>
    </div>
</div>

<script >
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
@endsection
