@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Factuur overzicht</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    @foreach($factuurs as $factuur)
                        {{$factuur->factuurId}}
                        @if($factuur->userId === session('user.id'))
                                Factuur:<a class="nav-link" href="{{ route('showFactuur', ['id' =>$factuur->factuurId]) }}">{{ __($factuur->date ) }}</a>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
